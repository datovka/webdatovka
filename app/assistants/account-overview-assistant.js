
function AccountOverviewAssistant() {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
}

AccountOverviewAssistant.prototype.activate = function(event) {
	var len_diff = this.controller.get("accountList").mojo.getLength() - 
				   accountList.length;
	console.log("lendiff "+ len_diff);
	if (len_diff > 0)
		// there are more items than should, remove excess
		this.controller.get("accountList").mojo.noticeRemovedItems(
			accountList.length-1, len_diff);
	/* show the accounts */
	this.controller.get("accountList").mojo.noticeUpdatedItems(0,
							accountList);
	if (event != null) {
		console.log(event.action);
	}											
	
};

AccountOverviewAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

AccountOverviewAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
};


AccountOverviewAssistant.prototype.handleAddAccountButtonPress = function(event){
	this.controller.stageController.pushScene({
		name: "account-config",
		sceneTemplate: "account-config/account-config-scene"},
		{
			account_index: null
		}
	);
};


AccountOverviewAssistant.prototype.handleAccountListTap = function(event){
	var idx = accountList.indexOf(event.item);
	this.controller.stageController.pushScene({
		name: "account-config",
		sceneTemplate: "account-config/account-config-scene"},
		{
			account_index: idx,
		}
	);
}

AccountOverviewAssistant.prototype.setup = function() {
	this.controller.setupWidget("AddAccountButton", {},
		{"label" : "Add account", "buttonClass" : "", "disabled" : false}
  	);
	Mojo.Event.listen(this.controller.get("AddAccountButton"), Mojo.Event.tap, 
     	  this.handleAddAccountButtonPress.bind(this));

  	this.controller.setupWidget("accountList",
        this.attributes = {
            itemTemplate: "account-overview/account-list-entry",
            listTemplate: "account-overview/account-list",
            swipeToDelete: false,
            reorderable: true,
         },
         this.model = {
             listTitle: "Accounts",
             items : accountList
          }
    ); 
	Mojo.Event.listen(this.controller.get("accountList"), Mojo.Event.listTap, 
     	  this.handleAccountListTap.bind(this));
};


