
function AccountDetailAssistant(args) {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
	  this.account_index = args.account_index;
}

AccountDetailAssistant.prototype.activate = function(event) {
	var account = accountList[this.account_index];
	if (account.info == "" || account.info == null) {
		console.log("Getting box info from web");
		this.controller.get("progress_spinner").mojo.start();
		isdsPlugin.async_data_download(account.username, account.password,
										account.type, "box_info");
	} else {
		console.log("Box info was in DB");
		this.data = JSON.parse(account.info);
		this.updateView();
	}
};


AccountDetailAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

AccountDetailAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	  Mojo.Event.stopListening(isdsPlugin.plugin, "isdsAsyncBoxInfoDownloadFinished", 
     	  this.isdsAsyncDownloadHandler);
};

	
AccountDetailAssistant.prototype.updateView = function(){
	var result = []; 
	var address = [];
	for (var key in this.data) {
		if (this.data[key] != "")
			if (key.slice(0,3) == "ad_")
				address.push({label: key.slice(3).replace(/_/g, " "),
							  value: this.data[key]});
			else
				result.push({label: key.replace(/_/g, " "),
							 value: this.data[key]});
	}
	this.controller.get("detailList").mojo.noticeUpdatedItems(0,
												result);
	this.controller.get("addressList").mojo.noticeUpdatedItems(0,
												address);
};


AccountDetailAssistant.prototype.isdsAsyncDownloadFinished = function(event) {
	var account = accountList[this.account_index];
	if (event.username == account.username) {
		console.log("Got async data for this account");
		this.controller.get("progress_spinner").mojo.stop();
		if (event.data.ok == 1) {
			this.data = event.data.data;
			accountList[this.account_index].info = JSON.stringify(this.data);
			console.log("Stored data: "+accountList[this.account_index].info);
			this.updateView();
		} else {
			Mojo.Controller.errorDialog(
				"It was not possible to download account info:\n"
				+event.data.error+"\nCheck your internet connection.");
		}
	} else {
		console.log("Got async data for different account: "+event.username);
	}
}


AccountDetailAssistant.prototype.setup = function() {
  	$('account_name').innerHTML = accountList[this.account_index].name;
	this.controller.setupWidget("detailList",
        this.attributes = {
            itemTemplate: "account-detail/detail-list-entry",
            listTemplate: "account-detail/detail-list",
            /*addItemLabel: "Add ...",*/
            swipeToDelete: false,
            reorderable: true,
         },
         this.model = {
			listTitle: "Account details",
            items : []
          }
    ); 
	this.controller.setupWidget("addressList",
        this.attributes = {
            itemTemplate: "account-detail/detail-list-entry",
            listTemplate: "account-detail/detail-list",
            /*addItemLabel: "Add ...",*/
            swipeToDelete: false,
            reorderable: true,
         },
         this.model = {
			listTitle: "Address",
            items : []
          }
    ); 
	// spinner				
	this.controller.setupWidget("progress_spinner",
		this.attributes = {
			//spinnerSize: "default"
		}, this.model = {
			spinning: false
		});
	// shared isds_interface element				
	this.isdsAsyncDownloadHandler = this.isdsAsyncDownloadFinished.bind(this);
	Mojo.Event.listen(isdsPlugin.plugin, "isdsAsyncBoxInfoDownloadFinished", 
     	  this.isdsAsyncDownloadHandler);
	document.body.appendChild(isdsPlugin.plugin);
};
