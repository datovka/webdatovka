
function MessageViewAssistant(args) {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
	  this.account = args.account;
	  this.messageType = args.type;
	  console.log("START: "+this.messageType);
	  this.messages = new Object();
}

MessageViewAssistant.prototype.loadMessagesFromDB = function () {
	var username = this.account.username;
	var message_type = this.messageType;
	var old_this = this;
	console.log("Username for selecting: " + username);
	messageDB.transaction(
		function(tx){
			tx.executeSql("SELECT * FROM messages WHERE\
			account=? AND type=?",
			[username, message_type],
			function(tx, rs){
				console.log("Found "+rs.rows.length+" messages for '"+
							username+"' and type "+message_type);
				for (var i = 0; i < rs.rows.length; i++) {
					var item = rs.rows.item(i);
					console.log("Data: "+item.data);
					old_this.messages[item.id] = JSON.parse(item.data);
				}
				old_this.updateMessageList();
				console.info("Messages loaded successfully");
			},
			function(tx, err){
				console.error("Messages *NOT* loaded: "
							  + err.message);
			})
		})
}

MessageViewAssistant.prototype.getMessages = function () {
	var items = [];
	for (var key in this.messages) {
		items.push(this.messages[key]);
		console.log(key);
	}
	return items;
}

MessageViewAssistant.prototype.dbSuccess = function (event) {
}

MessageViewAssistant.prototype.dbFailure = function (event) {
}

MessageViewAssistant.prototype.updateMessageList = function(event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
	  this.controller.get("messageList").mojo.noticeUpdatedItems(0,
														 this.getMessages());
}

MessageViewAssistant.prototype.activate = function(event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
	this.loadMessagesFromDB();
};

MessageViewAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

MessageViewAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	Mojo.Event.stopListening(isdsPlugin.plugin, "isdsAsyncListDownloadFinished", 
     	  this.isdsAsyncListDownloadHandler);
	Mojo.Event.stopListening(this.controller.get("messageList"), Mojo.Event.listTap, 
     	  this.messageTapHandler);
};

MessageViewAssistant.prototype.handleUpdateButtonPress = function(event){
	/* dispatch async request */
	this.controller.get("progress_spinner").mojo.start();
	var stat = isdsPlugin.async_data_download(
							this.account.username, this.account.password,
							this.account.type, this.messageType+"_list", "");
	console.log("ASYNC DISPATCH: " + stat);
 
};

MessageViewAssistant.prototype.isdsAsyncListDownloadFinished = function(event) {
	if (event.username == this.account.username &&
		event.messageType == this.messageType+"_list") {
		console.log("Got async data for this view");
		this.controller.get("progress_spinner").mojo.stop();
		if (event.data.ok == 1) {
			for (var i = 0; i < event.data.messages.length; i++) {
				var message = event.data.messages[i];
				this.messages[message.id] = message;
			}
			this.controller.get("messageList").mojo.noticeUpdatedItems(0,
														this.getMessages());
		} else {
			Mojo.Controller.errorDialog(
				"It was not possible to download the message list:\n"
				+event.data.error+"\nCheck your internet connection.");
		}
	} else {
		console.log("Got async data for different view: "+event.username+"; "
					+event.messageType);
	}
}


MessageViewAssistant.prototype.handleCommand = function(event) {
    if (event.type === Mojo.Event.command) {
        if (event.command == 'do-update') {
			this.handleUpdateButtonPress(null);
		} else {
			console.log(event.command);
		}
    }
}

MessageViewAssistant.prototype.handleMessageListTap = function(event){
	console.log("tap "+event.item.id);
	this.controller.stageController.pushScene({
		name: "message-detail",
		sceneTemplate: "message-detail/message-detail-scene"},
		{
			message: event.item,
			type: this.messageType,
			account: this.account,
		}
	);
}

MessageViewAssistant.prototype.setup = function() {
    $('message_type').innerHTML = this.messageType;
	$('account_name').innerHTML = this.account.name;
  	this.controller.setupWidget("messageList",
        this.attributes = {
            itemTemplate: "message-view/message-list-entry",
            listTemplate: "message-view/message-list",
            swipeToDelete: false,
            reorderable: true,
         },
         this.model = {
             //listTitle: "Received messages",
             items : []
          }
    );
	this.messageTapHandler = this.handleMessageListTap.bind(this);
	Mojo.Event.listen(this.controller.get("messageList"), Mojo.Event.listTap, 
     	  this.messageTapHandler);
	// command menu
	this.controller.setupWidget(Mojo.Menu.commandMenu, undefined, 
       		{items:	[{disable: true},
					 {label:'Update', command:'do-update', icon:'refresh'}
			    	]});
	// spinner				
	this.controller.setupWidget("progress_spinner",
		this.attributes = {
			//spinnerSize: "default"
		}, this.model = {
			spinning: false
		});
	// shared isds_interface element				
	document.body.appendChild(isdsPlugin.plugin);
	this.isdsAsyncListDownloadHandler = this.isdsAsyncListDownloadFinished.bind(this);
	Mojo.Event.listen(isdsPlugin.plugin, "isdsAsyncListDownloadFinished", 
     	  this.isdsAsyncListDownloadHandler);
};
