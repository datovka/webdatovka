
function FrontPageAssistant() {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
}


FrontPageAssistant.prototype.getAccountListData = function(accounts) {
	/* return the accounts */
	var result = [];
	for (var i = 0; i < accounts.length; i++) {
		result.push({'name': accounts[i].name, 'title': accounts[i].name,
					 'class':'info', 'detail': accounts[i].type, 'idx': i});
		result.push({'name': accounts[i].name, 'title': $L('Received'), 
					 'class':'received', 'detail': "", 'idx': i});
		result.push({'name': accounts[i].name, 'title': $L('Sent'), 
					 'class':'sent', 'detail': "", 'idx': i});
	}
	return result;
};

FrontPageAssistant.prototype.loadAccountDataFromDBSuccess = function(tx, rs){
	accounts = [];
	for (var i = 0; i < rs.rows.length; i++) {
		accounts.push(rs.rows.item(i));
	}
	accountList = accounts;
	this.updateAccountList();
}

FrontPageAssistant.prototype.loadAccountDataFromDBError = function(tx, error){
	console.error('An error occurred in webSQL: ' + error.message);
	Mojo.Controller.errorDialog('An error occurred in webSQL: ' + error.message);
}

// fires the update of account list
FrontPageAssistant.prototype.loadAccountDataFromDB = function() {
	var controller = this;
	accountDB.transaction(
		function(tx){
			tx.executeSql(
				'SELECT * FROM accounts',
				[],
				controller.loadAccountDataFromDBSuccess.bind(controller),
				controller.loadAccountDataFromDBError.bind(controller)
			);
		}
	);	
};

FrontPageAssistant.prototype.updateAccountList = function() {
	var len_diff = this.controller.get("accountList").mojo.getLength() - 
				   3*accountList.length;
	console.log("lendiff "+ len_diff);
	if (len_diff > 0)
		// there are more items than should, remove excess
		this.controller.get("accountList").mojo.noticeRemovedItems(
			accountList.length-1, len_diff);
	// place the items into the list
	this.controller.get("accountList").mojo.noticeUpdatedItems(0,
		this.getAccountListData(accountList));
	this.controller.get("accountList").mojo.getNodeByIndex(1)['detail'] = 5;
}

FrontPageAssistant.prototype.activate = function(event) {
	if (accountList.length == 0) 
		this.loadAccountDataFromDB();
	else
		this.updateAccountList();
};

FrontPageAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

FrontPageAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	Mojo.Event.stopListening(this.controller.get("accountList"), Mojo.Event.listTap, 
    	  this.accountTapHandler);
};




FrontPageAssistant.prototype.handleAccountListTap = function(event){
	if (event.item['class'] == 'info') {
		this.controller.stageController.pushScene({
			name: "account-detail",
			sceneTemplate: "account-detail/account-detail-scene"},
			{
				account_index: event.item.idx
			}
		);
	} else {
		this.controller.stageController.pushScene({
			name: "message-view",
			sceneTemplate: "message-view/message-view-scene"},
			{
				account: accountList[event.item.idx],
				type: event.item['class'],
			}
		);
	}
	
}

FrontPageAssistant.prototype.handleCommand = function(event) {
    console.log(event.command);
	if (event.type === Mojo.Event.command) {
        if (event.command == 'do-myAccounts') {
			this.controller.stageController.pushScene({
				name: "account-overview",
				sceneTemplate: "account-overview/account-overview-scene"
				});
		} else {
			console.log(event.command);
		}
    }
}

FrontPageAssistant.prototype.isdsReady = function(event) {
	isdsPlugin.isReady = true;
}


FrontPageAssistant.prototype.setup = function() {
	// setup some stage controller stuff for the whole application
	// shared isds_interface element				
	isdsPlugin.createElement(document);
	isdsPlugin.plugin = document.getElementById("isds_interface");
	isdsPlugin.plugin.ready = this.isdsReady.bind(this);
		
	// main menu
    this.controller.setupWidget(Mojo.Menu.appMenu,
	    this.attributes = {
	        omitDefaultItems: true
	    },
	    this.model = {
	        visible: true,
	        items: [ 
	            //{label: "About My App ...", command: 'do-myAbout'},
	            Mojo.Menu.editItem,
	            //{ label: "Preferences...", command: 'do-myPrefs' },
				{ label: "Accounts", command: 'do-myAccounts' }
	        ]
	    }
	);
	// setup the list
  	this.controller.setupWidget("accountList",
        this.attributes = {
            itemTemplate: "front-page/account-list-entry",
            listTemplate: "front-page/account-list",
            /*addItemLabel: "Add ...",*/
            swipeToDelete: false,
            reorderable: false,
         },
         this.model = {
             listTitle: "Accounts",
             items : []
          }
    ); 
	this.accountTapHandler = this.handleAccountListTap.bind(this);
	Mojo.Event.listen(this.controller.get("accountList"), Mojo.Event.listTap, 
     	  this.accountTapHandler);
	
}


