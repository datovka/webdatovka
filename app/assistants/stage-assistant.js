var isdsPlugin = null;
var accountDB = null;
var messageDB = null;
var accountList = [];


function StageAssistant() {
	/* this is the creator function for your stage assistant object */
}

StageAssistant.prototype.setup = function() {
	/* this function is for setup tasks that have to happen when the stage is first created */
     isdsPlugin = new isdsPluginModel();
	 this.openAccountDB();
	 this.openMessageDB();
	 this.controller.pushScene("front-page");

};


StageAssistant.prototype.openAccountDB = function() {
	/* this function is for setup tasks that have to happen when the stage is first created */
	accountDB = openDatabase("isds_accounts",
					"1", "webDatovka ISDS accounts", 50000);
	if (accountDB != null) {
		/*accountDB.transaction(function(tx){
			tx.executeSql("DROP TABLE IF EXISTS accounts",
			 [],
			 function(tx, rs){
				console.info("Account database setup complete");
			}, function(tx, err){
				console.error(err.message);
			})
		})*/
		accountDB.transaction(function(tx){
			tx.executeSql("CREATE TABLE IF NOT EXISTS accounts \
			(id INTEGER PRIMARY KEY, name TEXT, username TEXT,\
			 password TEXT, type TEXT, info TEXT);", [],
			 function(tx, rs){
				console.info("Account database setup complete");
			}, function(tx, err){
				console.error(err.message);
			})
		})
	} else {
		console.error("Account database was not opened");
	};
}

StageAssistant.prototype.openMessageDB = function() {
	/* this function is for setup tasks that have to happen when the stage is first created */
	messageDB = openDatabase("ext:webDatovka_messages",
		"1", "webDatovka message storage", 5000000);
	if (messageDB != null) {
		/*messageDB.transaction(function(tx){
			tx.executeSql("DROP TABLE IF EXISTS messages",
			 [],
			 function(tx, rs){
				console.info("Message database setup complete");
			}, function(tx, err){
				console.error(err.message);
			})
		});*/
		messageDB.transaction(function(tx){
			tx.executeSql("CREATE TABLE IF NOT EXISTS messages \
			(id INTEGER PRIMARY KEY, account TEXT, type INTEGER,\
			 data TEXT);", [],
			function(tx, rs){
				console.info("Message database setup complete");
			},
			function(tx, err){
				console.error(err.message);
			})
		});
		/*messageDB.transaction(function(tx){
			tx.executeSql("CREATE TABLE IF NOT EXISTS attachments \
			(id INTEGER PRIMARY KEY, message_id INTEGER, account TEXT,\
			 filename TEXT, path TEXT, size INTEGER);", [],
			function(tx, rs){
				console.info("Message database setup complete");
			},
			function(tx, err){
				console.error(err.message);
			})
		})*/
	} else {
		console.error("Message database was not opened");
	}
}


