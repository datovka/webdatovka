
function MessageDetailAssistant(args) {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
	  this.account = args.account;
	  this.message = args.message;
	  console.log("MESSAGE "+JSON.stringify(this.message));
	  this.messageType = args.type;
	  this.attachments = [];
	  this.detailFields = [["title","Subject","bold"],
	  					   ["sender","Sender",""],
	  					   ["recipient", "Recipient",""]];
}


MessageDetailAssistant.prototype.getDetails = function () {
	var ret = [];
	for (var i = 0; i < this.detailFields.length; i++) {
		var field = this.detailFields[i];
		//console.log(field);
		ret.push({name: field[1], value: this.message[field[0]], cls: field[2]});
	}
	ret.push({name: "Delivered", 
			  value: new Date(this.message["delivery_time"]*1000),
			  cls: ""});
	return ret;
}

MessageDetailAssistant.prototype.getAttachments = function(){
	console.log("ATTACH "+this.message.attachments);
	/* temporary solution for libnicds not handling complete
	 * sent messages
	 */
	console.log(this.messageType);
	if (this.messageType == "sent") {
		this.attachments = [{
			"title": "Download of attachments for sent messages is not yet supported"
		}];
		return
	}
	var download = false;
	if (this.attachments.length == 0) {
		// no attachments are available yet
		// try to fetch them from the database
		download = true;
		if (this.message.attachments != null && this.message.attachments.length > 0) {
			// we have attachments in the database
			this.attachments = this.message.attachments;
			console.log("attachments in DB");
			download = false;
		}
	}
	// check if stored attachments really exist
	for (var i = 0; i < this.attachments.length; i++) {
		var attach = this.attachments[i];
		var exists = isdsPlugin.plugin.check_file_exists(attach.path);
		if (exists != "1") {
			download = true;
			this.attachments = []
			break;
		}
	}
	if (download) {
		console.log("REDOWNLOAD NEEDED");
		// we have to download it
		this.controller.get("attachmentList").mojo.noticeUpdatedItems(0, [{
			title: "Downloading...",
			spinner: '<div id="attachment_spinner" x-mojo-element="Spinner"></div>'
		}]);
		// attachment spinner
		this.controller.setupWidget("attachment_spinner", this.attributes = {
			spinnerSize: "small"
		}, this.model = {
			spinning: false
		});
		this.controller.get('attachment_spinner').mojo.start();
		// ask for the data
		var stat = isdsPlugin.async_data_download(
							this.account.username, this.account.password,
							this.account.type, "complete_received", this.message.id);
		console.log("ASYNC DISPATCH: " + stat);
	}
}

MessageDetailAssistant.prototype.activate = function(event) {
	/* put in event handlers here that should only be in effect when this scene is active. For
	   example, key handlers that are observing the document */
	this.getAttachments();
	if (this.attachments.length != 0)
		this.controller.get("attachmentList").mojo.noticeUpdatedItems(0,
														 this.attachments);
};

MessageDetailAssistant.prototype.asyncDownloadCallback = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	console.log("ASYNC DATA: "+event.data);
	if (this.message.id != event.msg_id || this.account.username != event.username) {
		console.error("Handler called for bad message: "+event.msg_id+" login: "+event.username);
		console.error("This is : "+this.message.id+" login: "+this.account.username);
		return;
	}
	var data = event.data;
	if (data.ok == 1) {
		this.message = data.message;
		this.attachments = this.message.attachments;
		if (this.attachments.length != 0)
			this.controller.get("attachmentList").mojo.noticeUpdatedItems(0,
															 this.attachments);
	} else {
		this.controller.get("attachmentList").mojo.noticeUpdatedItems(0,
				[{title: "Could not download attachments: " + data.error}]);
		console.error("Could not download attachments: " + data.error);
	}
	//this.controller.get('attachment_spinner').mojo.stop();
};


MessageDetailAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

MessageDetailAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	console.log("CLEANUP: "+this.message.id);
	Mojo.Event.stopListening(isdsPlugin.plugin, "isdsAsyncDownloadFinished", 
     	  this.isdsAsyncDownloadHandler);
	Mojo.Event.stopListening(this.controller.get("attachmentList"), Mojo.Event.listTap, 
     	  this.attachmentTapHandler);
};


MessageDetailAssistant.prototype.handleAttachmentListTap = function(event){
	if (event.item.path != null) {
		this.controller.serviceRequest('palm://com.palm.applicationManager', {
			method: 'open',
			parameters: {
				target: "file://" + event.item.path
			// "file:///media/internal/"
			}
		});
	}
}   


MessageDetailAssistant.prototype.setup = function() {
    $('message_type').innerHTML = this.messageType;
	$('account_name').innerHTML = this.account.name;
  	this.controller.setupWidget("detailList",
        this.attributes = {
            itemTemplate: "message-detail/detail-list-entry",
            listTemplate: "message-detail/detail-list",
            swipeToDelete: false,
            reorderable: false,
         },
         this.model = {
             listTitle: "Info",
             items : this.getDetails(),
          }
    );
	this.controller.setupWidget("attachmentList",
        this.attributes = {
            itemTemplate: "message-detail/attachment-list-entry",
            listTemplate: "message-detail/attachment-list",
            swipeToDelete: false,
            reorderable: false,
         },
         this.model = {
             listTitle: "Attachments",
             items : [],
          }
    );
	this.attachmentTapHandler = this.handleAttachmentListTap.bind(this); 
	Mojo.Event.listen(this.controller.get("attachmentList"), Mojo.Event.listTap, 
     	  this.attachmentTapHandler);
	// shared isds_interface element	  
	document.body.appendChild(isdsPlugin.plugin);
	this.isdsAsyncDownloadHandler = this.asyncDownloadCallback.bind(this);
	Mojo.Event.listen(isdsPlugin.plugin, "isdsAsyncDownloadFinished", 
     	  this.isdsAsyncDownloadHandler);
};
