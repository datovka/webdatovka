
function AccountConfigAssistant(args) {
	/* this is the creator function for your scene assistant object. It will be passed all the 
	   additional parameters (after the scene name) that were passed to pushScene. The reference
	   to the scene controller (this.controller) has not be established yet, so any initialization
	   that needs the scene controller should be done in the setup function below. */
	this.account_index = args.account_index;
};

AccountConfigAssistant.prototype.activate = function(event) {
};

AccountConfigAssistant.prototype.deactivate = function(event) {
	/* remove any event handlers you added in activate and do any other cleanup that should happen before
	   this scene is popped or another scene is pushed on top */
};

AccountConfigAssistant.prototype.cleanup = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
};

AccountConfigAssistant.prototype.handleOKButtonTap = function(event) {
	var a = {}
	a.name = this.accountNameModel.value;
	a.username = this.usernameModel.value;
	a.password = this.passwordModel.value;
	a.type = (this.testAccountModel.value == true) ? "test":"normal";
	// update or create new
	if (this.account_index != null) {
		a.id = accountList[this.account_index].id;
		accountList[this.account_index] = a;
		// update the database
		accountDB.transaction(
			function(tx){
				tx.executeSql("UPDATE accounts \
				SET name=?, username=?, password=?, type=? \
				WHERE id=?;",
				[a.name, a.username, a.password, a.type, a.id],
				function(tx, rs){
					console.info("Account database update complete");
				},
				function(tx, err){
					console.error("Account database update: " + err.message);
				})
			})
	} else {
		accountList.push(a);
		accountDB.transaction(
			function(tx){
				tx.executeSql("INSERT INTO accounts \
				(name, username, password, type, info) \
				VALUES \
				(?,?,?,?,\"\")",
				[a.name, a.username, a.password, a.type],
				function(tx, rs){
					console.info("Account database entry created");
				},
				function(tx, err){
					console.error("Account database entry *NOT* created: "
								  + err.message);
				})
		})
	}
	// end this stage and return to the previous one	
	this.controller.stageController.popScene({action: 'add_account', ok: 1});
};

AccountConfigAssistant.prototype.handleCancelButtonTap = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	this.controller.stageController.popScene({action: 'add_account', ok: 0});
};

AccountConfigAssistant.prototype.handleRemoveAccountButtonTap = function(event) {
	/* this function should do any cleanup needed before the scene is destroyed as 
	   a result of being popped off the scene stack */
	var id = accountList[this.account_index].id;
	console.log("Deleting: " + id + " " + (id == 3));
	accountDB.transaction(
		function(tx){
			tx.executeSql("DELETE FROM accounts WHERE id=?",
			[id],
			function(tx, rs){
				console.info("Account database entry removed - " + rs.rowsAffected);
			},
			function(tx, err){
				console.error("Account database entry *NOT* removed: "
							  + err.message);
			})
		})
	accountList.splice(this.account_index, 1);
	this.controller.stageController.popScene({action: 'remove_account', ok: 1});
};

AccountConfigAssistant.prototype.setup = function() {
	var account =  accountList[this.account_index];
	var name = (account != null) ? account.name : "";
	var username = (account != null) ? account.username : "";
	var password = (account != null) ? account.password : "";
	var test = (account != null && account.type == "test") ?
															true : false;
	
	this.accountNameModel = {value: name};
	this.controller.setupWidget('accountNameField', {focus: true, 
							hintText: 'Your name for the account...'}, 
							this.accountNameModel);
	this.usernameModel = {value: username};
	this.controller.setupWidget('usernameField', {focus: false,
							hintText: 'Your login name...'},
							this.usernameModel);
	this.passwordModel =  {value: password};
	this.controller.setupWidget('passwordField', {focus: false,
							hintText: 'Your password...'},
							this.passwordModel);
	this.testAccountModel = {value: test};
	this.controller.setupWidget('testAccountCheckBox', {focus: false,
							hintText: 'Is this a test account?'}, 
							this.testAccountModel);
	this.controller.get('okButton').addEventListener(Mojo.Event.tap,
				this.handleOKButtonTap.bindAsEventListener(this));
	this.controller.get('cancelButton').addEventListener(Mojo.Event.tap,
				this.handleCancelButtonTap.bindAsEventListener(this));
	if (account != null) {
		this.controller.get('removeAccountButton').addEventListener(
			Mojo.Event.tap,
			this.handleRemoveAccountButtonTap.bindAsEventListener(this));
	} else {
		$('removeAccountButton').hide();
	}
					
	$("account_name").innerHTML = (this.account_index != null) ?
							account.name : "New account";						
};
