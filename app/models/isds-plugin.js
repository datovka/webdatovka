
function isdsPluginModel(){
    this.isReady = false;
	this.plugin = false;
}

isdsPluginModel.prototype.createElement = function(document)
{
    var pluginObj = document.createElement("object");
    
    pluginObj.id = "isds_interface";
    pluginObj.type = "application/x-palm-remote";
    pluginObj.width = 0;
    pluginObj.height = 0;
    pluginObj['x-palm-pass-event'] = true;
    
    var param1 = document.createElement("param");
    param1.name = "appid";
    param1.value = "cz.nic.labs.webdatovka";
    var param2 = document.createElement("param");
    param2.name = "exe";
    param2.value = "plugin/isds_interface";

    pluginObj.appendChild(param1);
    pluginObj.appendChild(param2);
	
	pluginObj.asyncDownloadMessageCallback =
			this.asyncDownloadMessageCallback.bind(this);
	pluginObj.asyncDownloadListCallback = 
			this.asyncDownloadListCallback.bind(this);
	pluginObj.asyncDownloadBoxInfoCallback = 
			this.asyncDownloadBoxInfoCallback.bind(this);

    document.body.appendChild(pluginObj);
}


isdsPluginModel.prototype.get_received_messages = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.get_received_messages.apply(this.plugin, arguments);
	}
	return null;
}


isdsPluginModel.prototype.get_sent_messages = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.get_sent_messages.apply(this.plugin, arguments);
	}
	return null;
}


isdsPluginModel.prototype.get_complete_received_message = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.get_complete_received_message.apply(this.plugin, arguments);
	}
	return null;
}

isdsPluginModel.prototype.async_data_download = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.async_data_download.apply(this.plugin, arguments);
	}
	return null;
}



isdsPluginModel.prototype.get_box_info = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.get_box_info.apply(this.plugin, arguments);
	}
	return null;
}

isdsPluginModel.prototype.check_file_exists = function()
{
    if (this.plugin) {
		while(!this.isReady) {
		} // wait until the plugin is ready
		return this.plugin.check_file_exists.apply(this.plugin, arguments);
	}
	return null;
}

isdsPluginModel.prototype.asyncDownloadMessageCallback = 
	function(username, msg_id, data)
{
	console.log("ASYNC DATA RAW "+data);
	// update database
	console.log("Storing message with attachments in DB");
	var parsed = JSON.parse(data);
	if (parsed.ok == 1) {
		var to_store = JSON.stringify(parsed.message);
		messageDB.transaction(function(tx){
			console.log(data);
			tx.executeSql("UPDATE messages \
				SET data=? WHERE id=? AND account=?;", [to_store, msg_id, username],
				function(tx, rs){
					console.info("Message database updated with attachemnts");
				}, function(tx, err){
					console.error("Message db update error: " + err.message);
				})
			})
	}
	// here we handle the database stuff and the just send the event out
	Mojo.Event.send(this.plugin, "isdsAsyncDownloadFinished",
	{username: username, msg_id: msg_id, data: parsed});

}

isdsPluginModel.prototype.asyncDownloadListCallback = function(username, type, data)
{
	console.log("ASYNC LIST DATA RAW "+data);
	// update database
	//console.log("Storing messages with attachments in DB");
	var parsed = JSON.parse(data);
	var msg_type = type.replace("_list","");
	// here we handle the database stuff
	if (parsed.ok == 1) {
		for (var i = 0; i < parsed.messages.length; i++) {
			var	json_data = JSON.stringify(parsed.messages[i]);
			var msg_id = parsed.messages[i].id;
			messageDB.transaction(
				(function(msg_id, username, msg_type, json_data){
					return function(tx){
						tx.executeSql("INSERT INTO messages \
							(id, account, type, data) \
							VALUES \
							(?,?,?,?)", [msg_id, username, msg_type, json_data],
							function(tx, rs){
								console.info("Message " + msg_id + " stored successfully");
							},
							function(tx, err){
								console.error("Message " + msg_id + " *NOT* stored: " +
							err.message);
						})
					}
				})(msg_id, username, msg_type, json_data)
			)
		}
	}
	// then just send the event out
	Mojo.Event.send(this.plugin, "isdsAsyncListDownloadFinished",
	{username: username, messageType: type, data: parsed});

}

isdsPluginModel.prototype.asyncDownloadBoxInfoCallback = function(username, data)
{
	console.log("ASYNC BOX INFO DATA RAW "+data);
	var parsed = JSON.parse(data);
	if (parsed.ok == 1) {
		// update database
		console.log("Storing account details in DB");
		accountDB.transaction(
		function(tx){
			tx.executeSql("UPDATE accounts \
			SET info=? WHERE username=?;",
			[JSON.stringify(parsed.data), username],
			function(tx, rs){
				console.info("Account database update complete");
			},
			function(tx, err){
				console.error("Account database update: " + err.message);
			})
		})
	}
	Mojo.Event.send(this.plugin, "isdsAsyncBoxInfoDownloadFinished",
					{username: username, data: parsed});

}
