#include <GLES2/gl2.h>
#include "SDL.h"
#include "PDL.h"
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
/* the nicds library */
#include "nicds.h"

typedef struct {
    char *login;
    char *password;
    int testing;
    char *method;
    char *msg_id;
} download_request;

download_request *DOWNLOAD_REQUEST = NULL;
download_request *WAITING_DOWNLOAD_REQUEST = NULL;

void wlog(const char* format, ...) {
    FILE *f = fopen("/media/internal/webdatovka.log", "a");
    va_list argptr;
    va_start(argptr, format);
    vfprintf(f, format, argptr);
    va_end(argptr);
    fclose(f);
}

/*
 * when INTERFACE_TEST is defined, we compile it without using the JS interface
 * but rather for debugging using prints, etc.
 */

#ifdef INTERFACE_TEST
#define ISDS_CONNECTION_NEW(parms)		\
    (isds_conn_new("", ISDS_HOST_TESTING))
#else
#define ISDS_CONNECTION_NEW(parms)					\
    (isds_conn_new("", (strcmp(PDL_GetJSParamString(parms, 2),"test") == 0) ? \
		   ISDS_HOST_TESTING:ISDS_HOST_RELEASE))
#endif

#ifdef INTERFACE_TEST
#define ISDS_CONNECTION_INIT(conn, parms)			\
    (isds_conn_set_credentials(conn, "7io3jf", "Schr8ne4ka1"))
#else
#define ISDS_CONNECTION_INIT(conn, parms)				\
    (isds_conn_set_credentials(conn, PDL_GetJSParamString(parms, 0),	\
			       PDL_GetJSParamString(parms, 1)))
#endif

#ifdef INTERFACE_TEST
PDL_Err PDL_JSReply(PDL_JSParameters *parms, const char *result) {
    printf("DEBUG: PDL_JSReply: %s", result);
}
#endif

/* macro returning a string value or "" when the value is NULL */
#define to_string(value) ((value != NULL) ? value : "")



void process_download_request_box_info(const download_request *req)
{
    wlog("Starting async box info download: %s\n", req->login);
    struct isds_conn *conn;
    const char *return_params[2];
    return_params[0] = req->login;
    conn = isds_conn_new("", req->testing ? ISDS_HOST_TESTING:ISDS_HOST_RELEASE);
    isds_conn_set_credentials(conn, req->login, req->password);
    /* stazeni informaci o schrance */
    struct isds_box *box = ws_get_owner_info_from_login(conn);
    if (box != NULL) {
	/* construction of the result string */
	int res_size = 512;
	int written;
	char *result, *tmp;
	if ((result = malloc(res_size)) == NULL) {
	    return_params[1] = "{\"ok\": 0, \"error\": \""
			"Could not allocate memory for result!\"}";
	    PDL_Err mjErr = PDL_CallJS("asyncDownloadBoxInfoCallback",
				       return_params, 2);
	} else {
	    while (1) {
		written = snprintf(result, res_size, 
				   "{\"ok\": 1, \"data\": {\"ID\": \"%s\","
				   "\"first_name\":\"%s\", \"last_name\": \"%s\","
				   "\"firm_name\": \"%s\", \"ic\": \"%s\","
				   "\"ad_city\": \"%s\", \"ad_street\": \"%s\","
				   "\"ad_number_in_street\": \"%s\","
				   "\"ad_number_in_municipality\": \"%s\","
				   "\"ad_zip_code\": \"%s\", \"ad_state\": \"%s\","
				   "\"bi_date\": \"%s\", \"bi_state\": \"%s\"}}",
				   box->id,
				   to_string(box->first_name),
				   to_string(box->last_name),
				   to_string(box->firm_name),
				   to_string(box->ic),
				   to_string(box->ad_city),
				   to_string(box->ad_street),
				   to_string(box->ad_number_in_street),
				   to_string(box->ad_number_in_municipality),
				   to_string(box->ad_zip_code),
				   to_string(box->ad_state),
				   to_string(box->bi_date),
				   to_string(box->bi_state)
				   );
		if (written < res_size) {
		    return_params[1] = result;
		    PDL_Err mjErr = PDL_CallJS("asyncDownloadBoxInfoCallback",
					       return_params, 2);
		    free(result);
		    isds_box_delete(box);
		    break;
		} else {
		    // the buffer was not big enough
		    wlog("Reallocating in box info: %d -> %d\n", res_size, written+1);
		    res_size = written + 1;
		    if ((tmp = realloc(result, res_size)) == NULL) {
			free(result);
			return_params[1] = "{\"ok\": 0, \"error\": \""
			    "Could not allocate memory for result!\"}";
			PDL_Err mjErr = PDL_CallJS("asyncDownloadBoxInfoCallback",
						   return_params, 2);
			break;
		    } else {
			result = tmp;
		    }
		}
	    }
	}
    } else {
	return_params[1] = "{\"ok\": 0, \"error\": \""
	    "Nezdarilo se stahnout informace o schrance!\"}";
	PDL_Err mjErr = PDL_CallJS("asyncDownloadBoxInfoCallback",
				   return_params, 2);
    }
    isds_conn_logout(conn);
    isds_conn_delete(conn);
}


void process_download_request_message_list(const download_request *req, 
	    struct isds_env_list* (*isds_list_func)(struct isds_conn*,
						    struct isds_msg_params*)) {
    struct isds_conn *conn;
    //usleep(5000000); // sleep 5 s to test qeueing of requests
    const char *return_params[3];
    return_params[0] = req->login;
    return_params[1] = req->method;
    conn = isds_conn_new("", req->testing ? ISDS_HOST_TESTING:ISDS_HOST_RELEASE);
    isds_conn_set_credentials(conn, req->login, req->password);
    struct isds_msg_params *params = isds_msg_params_new();
    int error = 0;
    struct isds_env_list *env_list = isds_list_func(conn, params);
    wlog("DOWNLOAD MESSAGE LIST\n");
    if (env_list == NULL) {
	return_params[2] = "{\"ok\": 0, \"error\": \""
	    "Nezdarilo se stahnout seznam zprav!\"}";
	PDL_Err mjErr = PDL_CallJS("asyncDownloadListCallback", return_params, 3);
    } else {
	wlog("DOWNLOAD MESSAGE LIST - SUCCESS\n");
	int i, pos = 0, res_size, wrt;
	res_size = 64 + env_list->count * 256; // guess the memory needed for the result
	char *result, *tmp;
	if ((result = malloc(res_size)) == NULL) {
	    return_params[2] = "{\"ok\": 0, \"error\": \""
		"Could not allocate memory for result!\"}";
	    PDL_Err mjErr = PDL_CallJS("asyncDownloadListCallback", return_params, 3);
	} else {
	    pos += sprintf(result, "{\"ok\": 1, \"messages\": [  ");
	    wlog("DOWNLOAD MESSAGE LIST - PROCESSING\n");
	    for (i = 0; i < env_list->count; i++) {
		struct isds_env *env = &(env_list->msgs[i]);
		long unsigned int acctm, delivtm;
		delivtm = (env->delivery_time != NULL) ? (long unsigned int)(*(env->delivery_time)) : 0;
		acctm = (env->acceptance_time != NULL) ? (long unsigned int)(*(env->acceptance_time)) : 0;
		while (1) {
		    wrt = snprintf((result+pos), res_size-pos,
				   "{\"id\":\"%s\",\"sender\":\"%s\",\"title\":"
				   "\"%s\",\"recipient\":\"%s\",\"delivery_time\":"
				   " %lu,\"acceptance_time\":%lu}, ",
				   env->id, env->sender, env->annotation,
				   env->recipient, delivtm, acctm);
		    if (wrt >= res_size-pos) {
			// could not write enough
			wlog("Reallocating %d -> %d\n", res_size, res_size*2);
			res_size *= 2;
			if ((tmp = realloc(result, res_size)) == NULL) {
			    // not enough memory
			    return_params[2] = "{\"ok\": 0, \"error\": \""
				"Could not allocate memory for result!\"}";
			    PDL_Err mjErr = PDL_CallJS("asyncDownloadListCallback",
						       return_params, 3);
			    error = 1;
			    free(result);
			    break;
			} else {
			    result = tmp;
			}
		    } else {
			wlog("DOWNLOAD MESSAGE LIST - WRITTEN OK\n");
			pos += wrt;
			break;
		    }
		}
		if (error)
		    break;
	    }
	    if (!error) {
		pos -= 2; /* remove the trailing comma and space, this has the extra
			     benefit of ensuring that ]} will fit in nicely */
		pos += sprintf((result+pos), "]}");
		result[pos+1] = 0;
		wlog("ASYNC DOWNLOAD LIST - NO ERROR, SENDING RESULT: %d bytes;\n",
		     strlen(result));
		return_params[2] = result;
		PDL_Err mjErr = PDL_CallJS("asyncDownloadListCallback", return_params, 3);
		free(result);
	    }
	    wlog("ASYNC DOWNLOAD MESSAGE LIST - NO ERROR\n");
	    isds_env_list_delete(env_list);
	}
    }
    isds_conn_logout(conn);
    isds_conn_delete(conn);
}



void process_download_request_complete_message(const download_request *req) {
    struct isds_conn *conn;
    const char *return_params[3];
    return_params[0] = req->login;
    return_params[1] = req->msg_id;
    conn = isds_conn_new("", req->testing ? ISDS_HOST_TESTING:ISDS_HOST_RELEASE);
    isds_conn_set_credentials(conn, req->login, req->password);
    int error = 0;
    struct isds_msg *msg = ws_message_download(conn, req->msg_id);
    if (msg == NULL) {
	// call javascript
	wlog("nejde to stahnout\n");
	return_params[2] = "{\"ok\": 0, \"error\": \"Nezdarilo se stahnout zpravu!\"}";
	PDL_Err mjErr = PDL_CallJS("asyncDownloadMessageCallback", return_params, 3);
    } else {
	wlog("jde to stahnout\n");
	int i, pos = 0, res_size = 512, wrt;
	char *result, *tmp;
	if ((result = malloc(res_size)) == NULL) {
	    return_params[2] = "{\"ok\": 0, \"error\": \""
		"Could not allocate memory for result!\"}";
	    PDL_Err mjErr = PDL_CallJS("asyncDownloadMessageCallback", return_params, 3);
	} else {
	    long unsigned int acctm, delivtm;
	    delivtm = (msg->env.delivery_time != NULL) ? \
		(long unsigned int)(*(msg->env.delivery_time)) : 0;
	    acctm = (msg->env.acceptance_time != NULL) ? \
		(long unsigned int)(*(msg->env.acceptance_time)) : 0;
	    while (1) {
		wrt = snprintf(result,
			       res_size,
			       "{\"ok\": 1, \"message\": {\"id\": \"%s\","
			       " \"title\": \"%s\","
			       " \"sender\": \"%s\","
			       " \"recipient\": \"%s\","
			       " \"delivery_time\": %lu,"
			       " \"acceptance_time\": %lu,"
			       " \"attachments\": [   ",
			       msg->env.id, msg->env.annotation, msg->env.sender,
			       msg->env.recipient, delivtm, acctm);
		if (wrt >= res_size) {
		    // buffer was too small, we need to expand
		    wlog("REALLOC %d -> %d\n", res_size, res_size*2);
		    res_size *= 2;
		    if ((tmp = realloc(result, res_size)) == NULL) {
			// bad
			return_params[2] = "{\"ok\": 0, \"error\": \""
			    "Could not allocate memory for result!\"}";
			PDL_Err mjErr = PDL_CallJS("asyncDownloadMessageCallback",
						   return_params, 3);
			error = 1;
			free(result);
			break;
		    } else {
			result = tmp;
		    }
		} else {
		    pos += wrt;
		    break;
		}
	    }
	    if (!error) {
		/* attachments */
		/* webdatovka dir */
		int dir_ready = mkdir("/media/internal/webdatovka/", S_IRWXU);
		if (dir_ready == 0 || errno == EEXIST) {
		    /* save the attachments */
		    struct isds_file *f;
		    int filenum;
		    for (filenum = 0; filenum < msg->file_count; filenum++) {
		        f = &(msg->files[filenum]);
			int len = strlen("/media/internal/webdatovka/")+strlen(req->msg_id)+
			    strlen(req->login) + strlen(f->file_descr) + 3;
			char *fname = calloc(len, 1);
			fname = strcat(fname, "/media/internal/webdatovka/");
			fname = strcat(fname, req->login);
			fname = strcat(fname, "/");
			dir_ready = mkdir(fname, S_IRWXU); // account dir
			fname = strcat(fname, req->msg_id);
			fname = strcat(fname, "/");
			dir_ready = mkdir(fname, S_IRWXU); // message dir
			fname = strcat(fname, f->file_descr);
			/* check for outfile existence */
			FILE *outfile = fopen(fname, "r");
			int save_it = 1;
			int ok = 1;
			if (outfile) {
			    struct stat st;
			    fstat(fileno(outfile), &st);
			    if (st.st_size == f->size)
				save_it = 0; // we do not save
			    fclose(outfile);
			}
			if (save_it) {
			    FILE *outfile = fopen(fname, "w");
			    if (outfile) {
				fwrite(f->content, f->size, 1, outfile);
				fclose(outfile);
				ok = 1;
			    } else
				ok = 0;
			}
			while (1) {
			    wrt = snprintf((result+pos), res_size-pos,
					   "{\"path\": \"%s\", \"title\": \"%s\", "
					   "\"size\": %d, \"ok\": %d},  ",
					   fname, f->file_descr, f->size, ok);
			    if (wrt >= res_size-pos) {
				wlog("REALLOC %d -> %d\n", res_size, res_size*2);
				res_size *= 2;
				if ((tmp = realloc(result, res_size)) == NULL) {
				    return_params[2] = "{\"ok\": 0, \"error\": \""
					"Could not allocate memory for result!\"}";
				    PDL_Err mjErr = PDL_CallJS("asyncDownloadMessageCallback",
							       return_params, 3);
				    error = 1;
				    free(result);
				    break;
				} else {
				    result = tmp;
				}
			    } else {
				pos += wrt;
				break;
			    }
			}
			free(fname);
		    }
		    if (!error) {
			pos -= 3;
			pos += sprintf((result+pos), "]}}");
			result[pos+1] = 0;
			// call javascript
			return_params[2] = result;
			wlog("Sending async request. Data size: %d\n", strlen(result));
			PDL_Err mjErr = PDL_CallJS("asyncDownloadMessageCallback",
						   return_params, 3);
			if ( mjErr != PDL_NOERROR ) {
			    wlog("%s\n", PDL_GetError());
			} else {
			    wlog("Async ok\n");
			}
			free(result);
		    }
		}
	    }
	}
	isds_msg_delete(msg);
    }
    isds_conn_logout(conn);
    isds_conn_delete(conn);
}



PDL_bool check_file_exists(PDL_JSParameters *parms)
{
    const char *fname = PDL_GetJSParamString(parms, 0);
    FILE *outfile = fopen(fname, "r");
    char *result;
    if (outfile) {
	result = "1";
	fclose(outfile);
    } else 
	result = "0";
    PDL_JSReply(parms, result);
}


download_request* download_request_new(const char *login, const char *password,
				       const char *testing, const char *method,
				       const char *msg_id) {
    download_request *req = malloc(sizeof(download_request));
    req->login = malloc(strlen(login)+1);
    req->login = strcpy(req->login, login);
    req->password = malloc(strlen(password)+1);
    req->password = strcpy(req->password, password);
    req->msg_id = malloc(strlen(msg_id)+1);
    req->msg_id = strcpy(req->msg_id, msg_id);
    req->method = malloc(strlen(method)+1);
    req->method = strcpy(req->method, method);
    req->testing = (strcmp(testing, "test") == 0) ? 1 : 0;
    char buf[100];
    sprintf(buf, "REQUEST: %s; %s; %s; %s; %d\n",
	    req->login, req->password, req->method, req->msg_id, req->testing);
    wlog(buf);
    return req;
}

void download_request_free(download_request *req) {
    free(req->login);
    free(req->password);
    free(req->msg_id);
    free(req->method);
    free(req);
}


PDL_bool async_data_download(PDL_JSParameters *parms) {
    const char *login = PDL_GetJSParamString(parms, 0);
    const char *password = PDL_GetJSParamString(parms, 1);
    const char *test = PDL_GetJSParamString(parms, 2);
    const char *method = PDL_GetJSParamString(parms, 3);
    const char *msg_id = PDL_GetJSParamString(parms, 4);
    if (DOWNLOAD_REQUEST && strcmp(DOWNLOAD_REQUEST->login, login) == 0 &&
	strcmp(DOWNLOAD_REQUEST->method, method) == 0 &&
	strcmp(DOWNLOAD_REQUEST->msg_id, msg_id) == 0) { 
	PDL_JSReply(parms, "TO UZ SE ZPRACOVAVA");
	return PDL_TRUE;
    }
    if (WAITING_DOWNLOAD_REQUEST && 
	strcmp(WAITING_DOWNLOAD_REQUEST->login, login) == 0 && 
	strcmp(WAITING_DOWNLOAD_REQUEST->method, method) == 0 &&
	strcmp(WAITING_DOWNLOAD_REQUEST->msg_id, msg_id) == 0) { 
	PDL_JSReply(parms, "TO UZ JE VE FRONTE");
	return PDL_TRUE;
    }
    download_request *req = download_request_new(login, password, test,
						 method, msg_id);
    if (DOWNLOAD_REQUEST != NULL) {
	/* there is already a request in process - put it into the waiting position
	   any potential WAITING_DOWNLOAD_REQUEST is discarded */
	if (WAITING_DOWNLOAD_REQUEST != NULL) {
	    wlog("discarding old WAITING_DOWNLOAD_REQUEST\n");
	    download_request_free(WAITING_DOWNLOAD_REQUEST);
	}
	wlog("setting new WAITING_DOWNLOAD_REQUEST\n");
	WAITING_DOWNLOAD_REQUEST = req;
    } else {
	wlog("setting new DOWNLOAD_REQUEST\n");
	DOWNLOAD_REQUEST = req;
    }
    PDL_JSReply(parms, "TADY TO JE");
    return PDL_TRUE;
}




void process_download_request() {
    if (DOWNLOAD_REQUEST) {
	download_request *req = DOWNLOAD_REQUEST;
	/* process the request */
	if (strcmp(req->method, "sent_list") == 0)
	    /* sent messages */
	    process_download_request_message_list(req,
					  &ws_get_list_of_sent_messages);
	    
	else if (strcmp(req->method, "received_list") == 0)
	    /* received messages */
	    process_download_request_message_list(req,
					  &ws_get_list_of_received_messages);
	else if (strcmp(req->method, "complete_received") == 0)
	    /* one message by ID */
	    process_download_request_complete_message(req);
	else if (strcmp(req->method, "box_info") == 0)
	    process_download_request_box_info(req);
	download_request_free(req);
	DOWNLOAD_REQUEST = WAITING_DOWNLOAD_REQUEST;
	WAITING_DOWNLOAD_REQUEST = NULL;
    }
}


#ifdef INTERFACE_TEST
int main(int argc, char **argv) {
    get_sent_messages(NULL);
}
#else
int main(int argc, char **argv) {
    SDL_Init(SDL_INIT_VIDEO);
    PDL_Err err;
    err = PDL_RegisterJSHandler("async_data_download",
				async_data_download);
    err = PDL_RegisterJSHandler("check_file_exists", check_file_exists);
    PDL_JSRegistrationComplete();
    // call a ready function to tell the JS end that the binary part is ready
    PDL_Err mjErr = PDL_CallJS("ready", NULL, 0);
    if ( mjErr != PDL_NOERROR ) {
	wlog("PDL error: %s\n", PDL_GetError());
    }
    // wait for events
    SDL_Event event;
    do {
	if (DOWNLOAD_REQUEST != NULL) {
	    // process download request
	    wlog("Starting async process\n");
	    process_download_request();
	} else {
	    usleep(100000);
	}
	SDL_PollEvent(&event);

    } while (event.type != SDL_QUIT);
}
#endif
